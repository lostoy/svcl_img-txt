import os
import argparse
import glob
import shutil


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='convert images to video')
    parser.add_argument('--sec', type=float, help='each image show time sec, default 1', default=1, dest="sec")
    parser.add_argument('--input_dir', help='folder of input images', dest="input_dir")
    parser.add_argument('--output', help='output video', dest='output')
    option = parser.parse_args()

    # ffmpeg will ignore the last frame, so make a copy first
    img_list = glob.glob(os.path.join(option.input_dir, '*.png'))
    img_list = sorted(img_list)
    last_img_path = img_list[-1]

    copy_last_img_path = os.path.join(option.input_dir, os.path.splitext(os.path.basename(last_img_path))[0]+'_copy.png')
    shutil.copy(last_img_path, copy_last_img_path)

    if not os.path.exists(os.path.dirname(option.output)):
        os.makedirs(os.path.dirname(option.output))

    # issue ffmpeg command
    cmd_str = "ffmpeg -y -framerate 1/{} -pattern_type glob -i '{}/*.png' -vf 'fps=24,format=yuv420p' {}".format(option.sec, option.input_dir,option.output)
    os.system(cmd_str)

    # remove tmp copy
    os.remove(copy_last_img_path)
