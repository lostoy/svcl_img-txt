__author__ = 'lostoy'
from PIL import Image, ImageDraw, ImageFont
import argparse
import glob
import os

import cv2
import numpy as np


def read_config_and_txt(txt_path, parser):

    # load lines of txt into a list
    txt_list = []
    conf_list = []

    if not os.path.exists(txt_path):
        txt_path = os.path.join(os.path.dirname(txt_path), 'default.txt')

    with open(txt_path) as f:
        # process the first line
        line = f.readline()
        line = line.rstrip('\n')
        txt_blk = []
        if line.startswith('//'):
            line = line.strip('/')
            conf_list.append(parser.parse_known_args(line.split())[0])
        else:
            conf_list.append(parser.parse_known_args()[0])
            txt_blk.append(line)

        for line in f:
            line = line.rstrip('\n')
            if line.startswith('//'):
                # end the block
                txt_list.append(txt_blk[:])

                # process config and start a new block
                line = line.lstrip('/')
                conf_list.append(parser.parse_known_args(line.split())[0])

                txt_blk = []
            else:
                txt_blk.append(line)

        # the last block
        txt_list.append(txt_blk)

    return txt_list, conf_list


def img_plus_txt(img, txt_list, conf_list, height, width):
    '''

    :param img:
    :param txt:
    :param height:
    :param width:
    :param txt_blk_opt_parser:
    :return:
    '''

    if width != -1 and height != -1:
        img = img.resize((width, height))

    (width, height) = img.size


    # create a blank text image
    txt = Image.new('RGBA', img.size, (255, 255, 255, 0))

    # get a drawing context
    draw = ImageDraw.Draw(txt)

    # draw text blocks
    for (txt_blk, txt_conf) in zip(txt_list, conf_list):
        # first pass: get the size of the block
        blk_w = 0
        blk_h = 0

        # read blk config
        font_size = int(txt_conf.font_size * height)
        rgba_color = (txt_conf.txt_r, txt_conf.txt_g, txt_conf.txt_b, txt_conf.txt_a)
        txt_pos = txt_conf.txt_pos

        # get a font
        font = ImageFont.truetype('./Ubuntu-B.ttf', font_size)
        for t in txt_blk:
            txt_size = draw.textsize(t, font)
            # add to blk size
            blk_w = max(blk_w, txt_size[0])
            blk_h += txt_size[1]

        # set the start position
        start_x = 0
        start_y = 0
        if txt_pos == 'topleft':
            start_x = 0
            start_y = 0
        elif txt_pos == 'topright':
            start_x = width-blk_w
            start_y = 0
        elif txt_pos == 'bottomleft':
            start_x = 0
            start_y = height-blk_h
        elif txt_pos == 'bottomright':
            start_x = width-blk_w
            start_y = height-blk_h

        # second pass:
        for t in txt_blk:

            txt_size = draw.textsize(t, font)

            if not t.isspace():
                draw.rectangle([(start_x, start_y), (start_x+txt_size[0], start_y+txt_size[1])], fill=(option.blk_r, option.blk_g, option.blk_b, option.blk_a))

            draw.text((start_x, start_y), t, font=font, fill=rgba_color)
            start_y += txt_size[1]

    out = Image.alpha_composite(img, txt)

    return out

if __name__ == '__main__':
    # a txt block option parser
    txt_blk_opt_parser = argparse.ArgumentParser(add_help=False)
    txt_blk_opt_parser.add_argument('--txt_pos', help='text postion', default="topleft", choices=["topleft", "topright", "bottomleft", "bottomright"], dest='txt_pos')
    txt_blk_opt_parser.add_argument('--font_size', type=float, help='text font size (relative to height)', default=0.08, dest='font_size')
    txt_blk_opt_parser.add_argument('--txt_r', type=int, help='text color R', default=255, dest='txt_r')
    txt_blk_opt_parser.add_argument('--txt_g', type=int, help='text color G', default=255, dest='txt_g')
    txt_blk_opt_parser.add_argument('--txt_b', type=int, help='text color B', default=255, dest='txt_b')
    txt_blk_opt_parser.add_argument('--txt_a', type=int, help='text color A(opacity)', default=255, dest='txt_a')

    txt_blk_opt_parser.add_argument('--blk_r', type=int, help='block color R', default=0, dest='blk_r')
    txt_blk_opt_parser.add_argument('--blk_g', type=int, help='block color G', default=0, dest='blk_g')
    txt_blk_opt_parser.add_argument('--blk_b', type=int, help='block color B', default=0, dest='blk_b')
    txt_blk_opt_parser.add_argument('--blk_a', type=int, help='block color A(opacity)', default=0, dest='blk_a')

    # parse arguments
    parser = argparse.ArgumentParser(parents=[txt_blk_opt_parser], description='superimpose text on images', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--width', type=int, help='resized image width, set to -1 if no resize', default=320, dest="width")
    parser.add_argument('--height', type=int, help='resized image height, set to -1 if no resize', default=240, dest="height")

    parser.add_argument('--txt_dir', help='folder of text, place a default.txt in this folder if you only have one label for all the frames', dest='txt_dir', required=True)

    parser.add_argument('--output_dir', help='folder of output superimposed image', dest='output_dir')

    # input group
    input_grp = parser.add_mutually_exclusive_group(required=True)
    input_grp.add_argument('--img_dir', help='folder of images', dest='img_dir')
    input_grp.add_argument('--vid_path', help='input video path', dest='vid_path')

    # video output legnth arguments
    vid_output_grp = parser.add_mutually_exclusive_group(required=False)
    vid_output_grp.add_argument('--sec', type=float, help='every image hold for sec in output video', dest='sec')
    vid_output_grp.add_argument('--length', type=float, help='output video length in sec', dest='length')

    # video input sample rate
    parser.add_argument('--in_fps', type=float, help='input video fps', dest='in_fps', default=12)

    # video output path
    parser.add_argument('--output_vid_path', help='directly output video path', dest='output_vid_path')

    option = parser.parse_args()

    # output setting
    if option.output_dir is not None and not os.path.exists(option.output_dir):
        os.makedirs(option.output_dir)
    if option.output_vid_path is not None:
        if cv2.__version__ == '3.0.0':
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        else:
            fourcc = cv2.cv.CV_FOURCC(*'mp4v')
        vid_writer = cv2.VideoWriter(option.output_vid_path, fourcc=fourcc, fps=24, frameSize=(option.width, option.height))

    # case when input is image dir
    if option.img_dir is not None:
        # get file lists
        img_path_list = list()
        img_path_list = img_path_list + glob.glob(os.path.join(option.img_dir, '*.png'))
        img_path_list = img_path_list + glob.glob(os.path.join(option.img_dir, '*.jpg'))
        img_path_list = img_path_list + glob.glob(os.path.join(option.img_dir, '*.bmp'))
        img_path_list = img_path_list + glob.glob(os.path.join(option.img_dir, '*.jpeg'))
        img_path_list = sorted(img_path_list)

        # frame duration
        if option.length is not None:
            length = option.length
        else:
            if option.sec is None:
                sec = 1
            else:
                sec = option.sec
            length = sec * len(img_path_list)

        sample_rep = int(length/len(img_path_list)*24)
        remainder = int(length*24 - sample_rep*len(img_path_list))

        for img_id, img_path in enumerate(img_path_list):
            img_name = os.path.splitext(os.path.basename(img_path))[0]
            txt_path = os.path.join(option.txt_dir, img_name+'.txt')

            img = Image.open(img_path).convert('RGBA')
            # load block of lines and config into a list
            txt_list, conf_list = read_config_and_txt(txt_path, txt_blk_opt_parser)

            out = img_plus_txt(img, txt_list, conf_list, option.height, option.width)
            if option.output_dir is not None:
                out.save(os.path.join(option.output_dir, img_name+'_p_txt.png'))

            if option.output_vid_path is not None:
                for i in np.arange(sample_rep + int(img_id < remainder)):
                    vid_writer.write(cv2.cvtColor(np.array(out).copy(), cv2.COLOR_BGRA2RGB))

    if option.vid_path is not None:
        vid_cap = cv2.VideoCapture(option.vid_path)

        # now assume only one label for the whole video
        txt_path = os.path.join(option.txt_dir, 'default.txt')

        vid_name = os.path.splitext(os.path.basename(option.vid_path))[0]

        frame_id = 0
        sample_id = 1
        if cv2.__version__ == '3.0.0':
            vid_cap_fps = vid_cap.get(cv2.CAP_PROP_FPS)
            vid_cap_frame_count = vid_cap.get(cv2.CAP_PROP_FRAME_COUNT)
        else:
            vid_cap_fps = vid_cap.get(cv2.cv.CV_CAP_PROP_FPS)
            vid_cap_frame_count = vid_cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)

        sample_rate = max(int(vid_cap_fps/option.in_fps), 1)
        n_sample = int(vid_cap_frame_count/sample_rate)

        if option.length is not None:
            length = option.length
        else:
            if option.sec is None:
                sec = 0.1
            else:
                sec = option.sec

            length = sec * n_sample
        sample_rep = int(length / n_sample * 24)
        remainder = int(length * 24 - sample_rep*n_sample)

        while True:
            ret, img = vid_cap.read()

            if not ret or img is None:
                break

            if frame_id % sample_rate != 0:
                frame_id += 1
                continue

            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)
            img_pil = Image.fromarray(img.copy())

            txt_list, conf_list = read_config_and_txt(txt_path, txt_blk_opt_parser)

            out = img_plus_txt(img_pil, txt_list, conf_list, option.height, option.width)

            if option.output_dir is not None:
                img_name = vid_name + str(sample_id)
                out.save(os.path.join(option.output_dir, img_name+'_p_txt.png'))

            if option.output_vid_path is not None:
                for i in np.arange(sample_rep+int(sample_id<remainder)):
                    vid_writer.write(cv2.cvtColor(np.array(out).copy(), cv2.COLOR_BGRA2RGB))

            frame_id += 1
            sample_id += 1

        if option.output_vid_path is not None:
            vid_writer.release()
