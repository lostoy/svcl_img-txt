import os
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='convert video to images')
    parser.add_argument('--fps', type=float, help='extract frame fps, default 1', default=1, dest="fps")
    parser.add_argument('--input', help='input video', dest="input")
    parser.add_argument('--output', help='seqeunce output images', dest='output')
    option = parser.parse_args()

    if not os.path.exists(os.path.dirname(option.output)):
        os.makedirs(os.path.dirname(option.output))

    cmd_str = 'ffmpeg -i {} -vf fps={} {}'.format(option.input, option.fps, option.output)
    print cmd_str
    os.system(cmd_str)

    # ffmpeg will duplicate the first image
    first_img_path = option.output % 1

    os.remove(first_img_path)